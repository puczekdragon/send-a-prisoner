﻿#Interactions relating to the education of children


#Offer a ward to a landed character
#This interaction is referenced in code! If it's renamed you have to ping a coder
give_someone_a_prisoner = {
	category = interaction_category_diplomacy

	desc = give_someone_a_prisoner_desc

	is_shown = {
		NOT = {
			scope:recipient = scope:actor
		}
		scope:recipient = {
			is_landed = yes
		}
		scope:actor = {
			NOT = {
				is_at_war_with = scope:recipient
			}
		}
	}

	send_name = SEND_PROPOSAL

	redirect = {
		scope:recipient = {
			if = {
				limit = {
					exists = employer
					NOT = { employer = scope:recipient }
				}
				save_scope_as = secondary_recipient
				employer = {
					save_scope_as = recipient
				}
			}
		}		
	}

	populate_actor_list = {
		scope:actor = {
			every_prisoner = {
				limit = {
				}
				add_to_list = characters
			}
		}
	}
	
	prompt = {
		desc = SEND_SELECTED_PRISONER
	}

	
	
	on_accept = {

		scope:secondary_actor = {
			release_from_prison = yes

		}

		scope:recipient = {
			imprison = {
				target = scope:secondary_actor
			}
			add_opinion = {
				target = scope:actor
				modifier = pleased_opinion
				opinion = 30
			}
		}

		

	}

	auto_accept = yes

}

ask_for_a_prisoner = {
	category = interaction_category_diplomacy

	desc = ask_for_a_prisoner_desc

	is_shown = {
		NOT = {
			scope:recipient = scope:actor
		}
		scope:recipient = {
			is_landed = yes
		}
		scope:actor = {
			NOT = {
				is_at_war_with = scope:recipient
			}
		}
	}

	send_name = SEND_PROPOSAL

	redirect = {
		scope:recipient = {
			if = {
				limit = {
					exists = employer
					NOT = { employer = scope:recipient }
				}
				save_scope_as = secondary_recipient
				employer = {
					save_scope_as = recipient
				}
			}
		}		
	}

	populate_actor_list = {
		scope:recipient = {
			every_prisoner = {
				limit = {
				}
				add_to_list = characters
			}
		}
	}
	
	prompt = {
		desc = SEND_SELECTED_PRISONER
	}

	
	on_accept = {

		scope:secondary_actor = {
			release_from_prison = yes

		}

		scope:actor = {
			imprison = {
				target = scope:secondary_actor
			}
		}
		if = {
			limit = { always = scope:gift }
			scope:actor = {
				pay_short_term_gold = {
					gold = bribe_value
					target = scope:recipient
				}
				stress_impact = {
					greedy = minor_stress_impact_gain
				}
			}
		}
		if = {
			limit = { always = scope:big_gift }
			scope:actor = {
				pay_short_term_gold = {
					gold = bribe_value
					target = scope:recipient
				}
				stress_impact = {
					greedy = minor_stress_impact_gain
				}
			}
		}
	}

	#Use hook
	send_option = {
		is_valid = {
			exists = scope:recipient
			scope:actor = {
				has_usable_hook = scope:recipient
			}
		}
		flag = hook
		localization = SCHEME_HOOK
	}
	should_use_extra_icon = {
		scope:actor = { has_usable_hook = scope:recipient }
	}
	extra_icon = "gfx/interface/icons/character_interactions/hook_icon.dds"

	send_option = {
		is_valid = {
			scope:actor = {
				gold >= bribe_value
			}
		}
		flag = gift
		localization = SCHEME_AGENT_GOLD
		current_description = {
			desc = SCHEME_AGENT_GOLD_VALID
		}
	}

	send_option = {	#very high bribe
		is_valid = {
			scope:actor = {
				gold >= bribe_value
			}
		}
		flag = big_gift
		localization = SCHEME_AGENT_EXTRA_GOLD
		current_description = {
			desc = SCHEME_AGENT_GOLD_VALID
		}
	}

	send_options_exclusive = no

	ai_accept = {
		base = -50
		opinion_modifier = { # Opinion
			opinion_target = scope:actor
			who = scope:recipient
			multiplier = 0.5
			desc = AI_OPINION_REASON
		}

		modifier = { # Rivals
			add = -300

			scope:recipient = {
				OR = {
					has_relation_rival = scope:actor
					has_relation_nemesis = scope:actor
					scope:recipient = {
						is_spouse_of = scope:actor
						exposed_cheating_on_spouse_trigger = { SPOUSE = scope:actor }
					}
				}
			}
			desc = "RIVAL_IS_ASKING"
		}

		modifier = { # Friend
			add = 100

			scope:recipient = {
				OR = {
					has_secret_relation_lover = scope:actor
					has_relation_lover = scope:actor
					has_relation_soulmate = scope:actor
					has_relation_friend = scope:actor
					has_relation_best_friend = scope:actor
				}
			}
			desc = "FRIEND_IS_ASKING"
		}

		modifier = { # At war
			add = -300
			scope:recipient = {
				is_at_war_with = scope:actor
			}
			desc = "IS_AT_WAR_REASON"
		}
		modifier = { # At war
			add = -300
			scope:recipient = {
				is_at_war_with = scope:secondary_recipient
			}
			desc = "IS_AT_WAR_WITH_TARGET_REASON"
		}

		modifier = {
			scope:hook = yes
			add = 100
			desc = SCHEME_WEAK_HOOK_USED
		}
		modifier = {
			scope:gift = yes
			add = 100
			desc = PROPOSED_CASH_FOR_PRISONER
		}
		modifier = {
			scope:big_gift = yes
			add = 50
			desc = PROPOSED_EXTRA_CASH_FOR_PRISONER
		}
		modifier = {
			AND = {
				OR = {
					scope:big_gift = yes
					scope:gift = yes
				}
				scope:recipient = {
					gold < -50
				}
			}
			
			add = 100
			desc = PROPOSED_CASH_WHEN_POOR
		}
		modifier = { # An Intimidated recipient is significantly more likely to accept an offer from the liege
			add = 40
			trigger = {
				scope:recipient = {
					target_is_liege_or_above = scope:actor
					has_dread_level_towards = {
						target = scope:actor
						level = 1
					}
				}
			}
			desc = INTIMIDATED_REASON
		}

		modifier = { # An Cowed recipient is significantly more likely to accept an offer from the liege
			add = 100
			trigger = {
				scope:recipient = {
					target_is_liege_or_above = scope:actor
					has_dread_level_towards = {
						target = scope:actor
						level = 2
					}
				}
			}
			desc = COWED_REASON
		}
		modifier = { # Vassal could be helpfull for their liege
			add = 15
			trigger = {
				scope:recipient = {
					is_vassal_of = scope:actor
				}
			}
			desc = IS_MY_LIEGE
		}

		modifier = { # Prisoner is close to me
			add = -250
			scope:secondary_actor = {
				OR = {
					is_spouse_of = scope:recipient
					is_close_family_of = scope:recipient
					has_relation_lover = scope:recipient
					has_relation_friend = scope:recipient
					has_secret_relation_lover = scope:recipient
					has_relation_soulmate = scope:recipient
					has_relation_best_friend = scope:recipient
					is_parent_of = scope:recipient
					has_relation_rival = scope:recipient
					has_relation_nemesis = scope:recipient
				}
			}
			desc = "IMPORTANT_CHAR_REASON"
		}

		modifier = { # Prisoner is same house as me
			add = -15
			AND = {
				scope:secondary_actor.house = scope:recipient.house
				NOT = {
					scope:actor.house = scope:recipient.house
				}
			}
			desc = "SAME_HOUSE_AS_ME"
		}

		modifier = { # Unimportant prisoner
			add = 20
			scope:secondary_actor = {
				NOR = {
					is_spouse_of = scope:recipient
					is_close_or_extended_family_of = scope:recipient
					has_relation_lover = scope:recipient
					has_relation_friend = scope:recipient
					has_secret_relation_lover = scope:recipient
					has_relation_soulmate = scope:recipient
					has_relation_best_friend = scope:recipient
					is_parent_of = scope:recipient
					has_relation_rival = scope:recipient
					has_relation_nemesis = scope:recipient
				}
			}
			scope:secondary_actor = {
				NOT = {
					is_landed = yes
				}
			}
			desc = "UNIMPORTANT_CHAR_REASON"
		}

		modifier = { # Is landed count
			add = -20
			scope:secondary_actor = {
				highest_held_title_tier = 2
			}
			desc = "IT_IS_COUNT"
		}

		modifier = { # Is landed duke
			add = -50
			scope:secondary_actor = {
				highest_held_title_tier = 3
			}
			desc = "IT_IS_DUKE"
		}

		modifier = { # Is landed king
			add = -70
			scope:secondary_actor = {
				highest_held_title_tier = 4
			}
			desc = "IT_IS_KING"
		}

		modifier = { # Is landed emperor
			add = -100
			scope:secondary_actor = {
				highest_held_title_tier = 5
			}
			desc = "IT_IS_EMPEROR"
		}

		modifier = { # It is my vassal
			add = -40
			scope:secondary_actor = {
				is_vassal_of = scope:recipient
			}
			desc = "THEY_ARE_MY_VASSAL"
		}

		modifier = { # Is torturer
			add = -60
			scope:recipient = {
				OR = {
					has_perk = torturer_perk
					ai_vengefulness > high_positive_ai_value
				}
			}
			desc = "I_LL_TORTURE_THEM"
		}

		modifier = { # Just arrived to my prison
			add = -25
			scope:secondary_recipient = {
				time_in_prison = { years < 2 }
			}
			desc = "TO_SHORT_IN_MY_PRISON"
		}

		modifier = { # To long in my prison
			add = 20
			scope:secondary_recipient = {
				time_in_prison = { years > 4 }
			}
			desc = "TO_LONG_IN_MY_PRISON"
		}

		modifier = { # Way to long in my prison
			add = 60
			scope:secondary_recipient = {
				time_in_prison = { years > 10 }
			}
			desc = "WAY_TO_LONG_IN_MY_PRISON"
		}
	}

}
